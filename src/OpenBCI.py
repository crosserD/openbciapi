from pylsl import StreamInfo, StreamOutlet
import numpy as np
from pyOpenBCI import OpenBCICyton
import config as cfg


class OpenBCI():

    def __init__(self):
        self.board = self.initialize_board()
        self.outlet_eeg = None
        self.outlet_aux = None

    def initialize_board(self):
        try:
            print('connecting to board...')
            board = OpenBCICyton(port=cfg.ports['dongle'])
        except:
            try:
                print('try different port')
                board = OpenBCICyton(port=cfg.ports['dongle'])
            except:
                print('ERROR Board not found')
                board = None
        return board


    def initialize_infostream(self):
        print("Creating LSL stream for EEG. \nName: OpenBCIEEG\nID: OpenBCItestEEG\n")
        info_eeg = StreamInfo('OpenBCIEEG', 'EEG', cfg.measurement['channels'], cfg.measurement['sampling_rate'], 'float32', 'OpenBCItestEEG')
        return StreamOutlet(info_eeg)

    def initialize_auxstream(self):
        print("Creating LSL stream for AUX. \nName: OpenBCIAUX\nID: OpenBCItestEEG\n")
        info_aux = StreamInfo('OpenBCIAUX', 'AUX', 3, 250, 'float32', 'OpenBCItestAUX')
        return StreamOutlet(info_aux)


    def lsl_stream_data(self):
        self.outlet_eeg = self.initialize_infostream()
        self.outlet_aux = self.initialize_auxstream()
        self.board.start_stream(self.lsl_streamers)

    def raw_print_data(self):
        self.board.start_stream(self.print_stream)

    def lsl_streamers(self, sample):
        self.outlet_eeg.push_sample(np.array(sample.channels_data)*cfg.scale['SCALE_FACTOR_EEG'])
        self.outlet_aux.push_sample(np.array(sample.aux_data)*cfg.scale['SCALE_FACTOR_AUX'])

    @staticmethod
    def print_stream(sample):
        print(sample.channels_data)


