measurement = {
    'channels': 8,
    'sampling_rate': 250

}

ports = {
    "dongle": "COM4"
}

scale = {
    'SCALE_FACTOR_EEG': (4500000) / 24 / (2 ** 23 - 1),  # uV/count
    'self.SCALE_FACTOR_AUX': 0.002 / (2 ** 4)
}